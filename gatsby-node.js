const axios = require("axios")

const fetchBeers = async () => {
  const { data } = await axios.get("https://api.punkapi.com/v2/beers")
  return data
}

exports.createPages = async ({ actions: { createPage } }) => {
  const beers = await fetchBeers()

  console.log(beers)

  createPage({
    path: "/",
    component: require.resolve("./src/templates/beers.js"),
    context: {
      beers,
    },
  })

  beers.forEach(beer => {
    createPage({
      path: `beers/${beer.id}`,
      component: require.resolve("./src/templates/beer.js"),
      context: {
        beer,
      },
    })
  })
}

import React from "react"

const Beer = ({ pageContext: { beer } }) => (
  <div>
    <h2>{beer.name}</h2>
    <img src={beer.image_url} />
  </div>
)

export default Beer

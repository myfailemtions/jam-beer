import React from "react"
import { Link } from "gatsby"

import "./style.css"

const renderBeerCard = ({ name, id }, key) => (
  <div key={key}>
    <h2>{name}</h2>
    <Link to={`/beers/${id}`}>Show more</Link>
  </div>
)

const BeersPage = ({ pageContext: { beers } }) => (
  <div className="beers">
    <ul>{beers.map(renderBeerCard)}</ul>
  </div>
)

export default BeersPage
